<?php

require_once('../vendor/autoload.php');

try {
	$dotenv = new Dotenv\Dotenv(dirname(__DIR__));
	$dotenv->load();
	$dotenv->required(['ENV','CDN_URL','SITES','SITE_ROUTES','DB_HOST','DB_NAME','DB_USER','DB_PASS']);
} catch (Exception $e) {
	exit('Could not find a .env file.');
}

// Path to your craft/ folder
$craftPath = '../craft';

// Environment value
// define('ENV', 'prod');

// Do not edit below this line
$path = rtrim($craftPath, '/').'/app/index.php';

if (! is_file($path)) {
	exit('Could not find your craft/ folder. Please ensure that <strong><code>$craftPath</code></strong> is set correctly in '.__FILE__);
}

require_once $path;