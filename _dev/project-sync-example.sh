#!/bin/bash

# Update the following variables for your project

SSH_SERVER="site.sandbox.us"
SSH_USER="site" # User account on remote server. devadmin if using old sandbox
SSH_KEY="/Users/Mal/.ssh/shiny" # Path to the private ssh key to use for auth
SSH_PORT="456"

REMOTE_DATABASE="site_cms"
REMOTE_DBUSER="site_dbusr"
REMOTE_DBPASS="idspispopd"
REMOTE_UPLOADS="/home/site/public_html/uploads/"

LOCAL_DATABASE="site"
LOCAL_DBUSER="dev_dbusr"
LOCAL_DBPASS="iddqd"
LOCAL_UPLOADS="/home/hive/site/public_html/uploads/"

# No further changes should be needed.

MYSQL_PATH="mysql"
MYSQLDUMP_PATH="mysqldump"

REMOTE_DBPORT="3306"
LOCAL_DBPORT="3306"
LOCAL_MIRRORPORT="33008"

echo "Opening SSH tunnel"

/usr/bin/ssh -f -o ExitOnForwardFailure=yes -L ${LOCAL_MIRRORPORT}:127.0.0.1:${REMOTE_DBPORT} ${SSH_USER}@${SSH_SERVER} -i ${SSH_KEY} -p ${SSH_PORT} sleep 10

# Store the process id for later termination

PID=$!

echo "Dumping remote database to remote_${REMOTE_DATABASE}.sql"

${MYSQLDUMP_PATH} -h 127.0.0.1 -P ${LOCAL_MIRRORPORT} -u ${REMOTE_DBUSER} -p${REMOTE_DBPASS} ${REMOTE_DATABASE} > backups/remote_${REMOTE_DATABASE}.sql

echo "Backing up local database to local_${LOCAL_DATABASE}.sql"

${MYSQLDUMP_PATH} -h 127.0.0.1 -P ${LOCAL_DBPORT} -u ${LOCAL_DBUSER} -p${LOCAL_DBPASS} ${LOCAL_DATABASE} > backups/local_${LOCAL_DATABASE}.sql

echo "Loading remote database"

${MYSQL_PATH} -h 127.0.0.1 -P ${LOCAL_DBPORT} -u ${LOCAL_DBUSER} -p${LOCAL_DBPASS} ${LOCAL_DATABASE} < backups/remote_${REMOTE_DATABASE}.sql

echo "Closing SSH tunnel"

echo "Syncing uploads"

rsync -avhr --delete -e "ssh -o Port=${SSH_PORT} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o IdentityFile=${SSH_KEY}" ${SSH_USER}@${SSH_SERVER}:${REMOTE_UPLOADS} ${LOCAL_UPLOADS}

if [ ! -z "$PID" ]; then
	kill $PID
fi