<?php

// Command for cron:
// cd /home/sitename/_dev; /usr/local/bin/php /home/sitename/_dev/taskWatcher.php

// FILL OUT THESE CREDENTIALS
define('MYSQL_HOST', '');
define('MYSQL_USER', '');
define('MYSQL_PASSWORD', '');
define('MYSQL_DATABASE', '');

class CheckTasks
{
	private static $db;

	private static function db()
	{
		if (! self::$db) {
			self::$db = mysqli_connect(
				MYSQL_HOST,
				MYSQL_USER,
				MYSQL_PASSWORD,
				MYSQL_DATABASE
			);
		}

		return self::$db;
	}

	public static function check()
	{
		self::db()->query('DELETE FROM craft_tasks WHERE status = "running" AND type = "DeleteStaleTemplateCaches";');
		self::db()->query('DELETE FROM craft_tasks WHERE status = "running" AND type = "Presto";');
	}

	public static function runPending()
	{
		system('../craft/app/etc/console/yiic taskmanager run');
	}
}

CheckTasks::check();
CheckTasks::runPending();