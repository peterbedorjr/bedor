<?php

// Override global routes with site-specific routes, if defined
if (defined('SITE')) {
	$routesFile = $_SERVER['DOCUMENT_ROOT'] . '/../craft/config/' . SITE . '.routes.php';

	if (file_exists($routesFile)) {
		require_once $routesFile;
	}
}

return $routes;