<?php

/* !!! IMPORTANT: Update the $appId and remove this comment before deploying to production. !!! */
$appId = 'craft';

// Get sites from ENV file
$sites = [];
foreach(explode(',', getenv('SITES')) as $i => $url) {
	$sites[$url] = [
		'site' => explode(',', getenv('SITE_ROUTES'))[$i]
	];
}

// Determine which site we are currently on
$domain = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : $_SERVER['HOSTNAME'];
$site = [
	'site' => isset($sites[$domain]) && ! empty($sites[$domain]) ? $sites[$domain]['site'] : 'main',
	'baseUrl' => $domain
];

define('SITE', $site['site']);
define('ENV', getenv('ENV'));

$secure = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on';
$protocol = $secure ? 'https://' : 'http://';

$baseRoot = $protocol . $site['baseUrl'];
$root = $protocol . $domain;

$cdnUrl = getenv('CDN_URL') ?: $root;
$assetUrl = $cdnUrl . '/assets';

$isPjax = isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] === 'true';

$manifest = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/assets/manifest.json'));

$config['*'] = [
	'actionTrigger' => 'trigger',
	'addTrailingSlashesToUrls' => false,
	'appId' => $appId,
	'allowAutoUpdates' => false,
	'baseSiteUrl' => $baseRoot,
	'cacheDuration' => false,
	'cacheElementQueries' => false,
	'cacheEnabled' => true,
	'cacheMethod' => php_sapi_name() === 'cli' ? 'db' : 'memcache',
	'convertFilenamesToAscii' => true,
	'cpTrigger' => 'cms',
	'csrfTokenName' => strtoupper($appId) . '_CSRF_TOKEN',
	'defaultImageQuality' => 80,
	'defaultSearchTermOptions' => ['subRight' => true],
	'enableCsrfProtection' => true,
	'errorTemplatePrefix' => SITE === 'main' ? '_errors/' : '_errors/' . SITE . '/',
	'generateTransformsBeforePageLoad' => true,
	'maxUploadFileSize' => 512000000,
	'omitScriptNameInUrls' => true,
	'phpSessionName' => $appId . 'SessionId',
	'postCpLoginRedirect' => 'entries',
	'preventUserEnumeration' => true,
	'rememberedUserSessionDuration' => 'P100Y',
	'sendPoweredByHeader' => false,
	'suppressTemplateErrors' => true,
	'siteUrl' => $root,
	'useCompressedJs' => true,
	'useEmailAsUsername' => true,
	'usePathInfo' => true,
	'manifest' => $manifest,

	// Global variables
	'env' => ENV,
	'site' => SITE,
	'assetUrl' => $assetUrl,
	'cdnUrl' => $cdnUrl,
	'dateFormat' => 'F j, Y',
	'isPjax' => $isPjax,
	'defaultLayout' => '_layouts/' . ($isPjax ? 'pjax' : 'master'),

	// Cache busting
	'cssVersion' => 1,
	'jsVersion' => 4,
	'jsLegacyVersion' => 1,

	// Environment variables
	'environmentVariables' => [
		'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
		'cdnUrl' => $cdnUrl
	],

	// Default entry values
	'entryDefaults' => [
		'title' => '',
		'seoTitle' => '',
		'seoDescription' => '',
		'seoNoIndex' => false
	]
];

switch (ENV) {
	case 'prod':
		// $config[$domain] = [];
		break;
	case 'stage':
		 $config[$domain] = [
		 	'suppressTemplateErrors' => false
		 ];
		break;
	case 'local':
		$config[$domain] = [
			'allowAutoUpdates' => true,
			'cacheMethod' => 'file',
			'devMode' => true,
			'suppressTemplateErrors' => false,
			'translationDebugOutput' => false,
			'useCompressedJs' => false
		];

		$localConfig = @include(CRAFT_CONFIG_PATH . '/local/general.php');

		$config[$domain] = array_merge($config[$domain], $localConfig);
}

return $config;
