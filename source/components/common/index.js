import { _body } from 'core/types';
import { RouteHandler } from 'wee-routes';
import $ from 'wee-dom';
import $events from 'wee-events';

const bindEvents = () => {
    $events.on('a', 'mousedown', (e, el) => {
        const $el = $(el);

        if ($el.css('outline-style') === 'none') {
            const outlineClass = '-no-outline';

            $el.addClass(outlineClass);

            $events.on($el, 'blur', () => {
                $el.removeClass(outlineClass);
            }, {
                once: true,
            });
        }
    }, {
        delegate: _body,
    });
}

export default new RouteHandler({
    init: bindEvents,
    update: bindEvents,
});
