import { RouteHandler } from 'wee-routes';
import Swiper from 'swiper';
import $ from 'wee-dom';
import hljs from '../../scripts/hljs';

let swiper;

export default new RouteHandler({
    init() {
        $('pre code').each(block => hljs.highlightBlock(block));

        if ($('.swiper-container').length) {
            swiper = new Swiper('.swiper-container', {
                autoHeight: true,
                pagination: {
                    el: '.swiper-pagination',
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        }
    },

    unload() {
        if ($('.swiper-container').length && swiper && typeof swiper.destory === 'function') {
            swiper.destroy();
        }
    }
});
