import $router, { RouteHandler } from 'wee-routes';
import $ from 'wee-dom';
import $fetch from 'wee-fetch';
import $events from 'wee-events';

const renderErrors = (errors) => `
    ${errors.map(error => `<li class="errors__error>${error}</li>`).join('')}
`

export default new RouteHandler({
    init() {
        $events.on('ref:contactForm', 'submit.contactForm', (e, el) => {
            $fetch.post('/', $(el).serialize())
                .then(({ data }) => {
                    $('ref:errors').html('');

                    if (data && data.error) {
                        const errors = Object.keys(data.error);

                        if (errors.length) {
                            errors.forEach(key => {
                                $(`name:${key}`).html(
                                    `<li class="errors__error">${data.error[key]}</li>`
                                );
                            });
                        }
                    } else if (data && data.success) {
                        $('ref:contact').html(`<h1>Thanks!</h1><h4>I'll get back to you as soon as I can.</h4>`);
                    }
                });

            e.preventDefault();
        });
    },
});
