import $router from 'wee-routes';
import './bootstrap';
import post from '../components/post';
import contactForm from '../components/contact-form';
import commonHandler from '../components/common';

const common = [commonHandler];

$router({
    transition: {
        target: '.main',
        class: '-is-loading',
        timeout: 100,
    },
}).pjax().map([
    {
        path: '/',
        handler: [...common],
    },
    {
        path: '/posts/:slug',
        handler: [post, ...common],
    },
    {
        path: '/categories',
        handler: [...common],
    },
    {
        path: '/categories/:slug',
        handler: [...common],
    },
    {
        path: '/about',
        handler: [...common],
    },
    {
        path: '/contact',
        handler: [contactForm, ...common],
    },
    {
        path: '/built-with',
        handler: [...common],
    },
]).run();
